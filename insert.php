<?php

// Create or open a database file
$db = new PDO('sqlite:jndex/jndex.sqlite3');
$db->exec("PRAGMA journal_mode=WAL;");
$db->exec("PRAGMA busy_timeout=3600000;");
$db->exec("PRAGMA auto_vacuum=0;");

// Creating a table
$db->exec("CREATE TABLE IF NOT EXISTS jndex (url text, artist text collate nocase, artist_id integer, song text collate nocase, song_id integer not null unique, album_art text, video_id text, artist_simplified text collate nocase, song_simplified text collate nocase, filename text collate nocase)");
$db->exec("CREATE VIRTUAL TABLE IF NOT EXISTS fts5 USING fts5(artist, song, content=jndex)");
$db->exec("CREATE TRIGGER if not exists fts5_insert AFTER INSERT ON jndex BEGIN INSERT INTO fts5(rowid, artist, song) VALUES (new.rowid, new.artist, new.song); END");

$json = json_decode(file_get_contents("php://stdin"),true);

// Prepare INSERT statement to SQLite3 file db
$insert = "INSERT or ignore INTO jndex (url,artist,artist_id,song,song_id,album_art,video_id,artist_simplified,song_simplified,filename) VALUES (:url,:artist,:artist_id,:song,:song_id,:album_art,:video_id,:artist_simplified,:song_simplified,:filename)";
$stmt = $db->prepare($insert);

// Bind parameters to statement variables
$stmt->bindParam(':url', $url);
$stmt->bindParam(':artist', $artist);
$stmt->bindParam(':artist_id', $artist_id);
$stmt->bindParam(':song', $song);
$stmt->bindParam(':song_id', $song_id);
$stmt->bindParam(':album_art', $album_art);
$stmt->bindParam(':video_id', $video_id);
$stmt->bindParam(':artist_simplified', $artist_simplified);
$stmt->bindParam(':song_simplified', $song_simplified);
$stmt->bindParam(':filename', $filename);

$db->beginTransaction();

// Insert all of the items in the array
foreach ($json as &$info) {
    $url = $info['url'];
    $artist = $info['artist'];
    $artist_id = $info['artist_id'];
    $song = $info['song'];
    $song_id = $info['song_id'];
    $album_art = $info['album_art'];
    $video_id = $info['video_id'];
    $artist_simplified = preg_replace('/^the/i','',transliterator_transliterate('Latin-ASCII; [\u0000-\u002f\u003a-\u0040\u005b-\u0060\u007b-\uffff] remove',$artist));
    $song_simplified = preg_replace('/^the/i','',transliterator_transliterate('Latin-ASCII; [\u0000-\u002f\u003a-\u0040\u005b-\u0060\u007b-\uffff] remove',$song));
    preg_match('/[^\/]*$/',$url,$filename); $filename = $filename[0];

    $stmt->execute();
}

$db->commit();

?>

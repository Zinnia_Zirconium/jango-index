#!/bin/bash
if [[ "$1" =~ ^[1-9][0-9]*$ ]] ; then
o="$1"
else
o="$( php o.php )"
fi
if [[ "$o" =~ ^[1-9][0-9]*$ ]] ; then
id="$o"
if [[ -f j/$id ]] ; then
if [[ ! -f j/${id}v ]] ; then
echo $id
curl -q -f -g -k -L -s -I -o /dev/null -c - -A 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4449.0 Safari/537.36' https://www.jango.com/stations/1/tunein?song_id=$id | curl -q -f -g -k -L -s -b - -A 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4449.0 Safari/537.36' https://www.jango.com/streams/info --compressed -o j/${id}v
grep -q -s ',"song_id":'"$id"',' j/${id}v || rm -f -v j/${id}v
grep -q -s ',"video_id":"' j/${id}v && jq -c -s . < j/${id}v | php update.php
mv -f -v j/${id}v j/$id
fi ; fi ; fi

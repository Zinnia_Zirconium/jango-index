<?php

$db = new PDO('sqlite:jndex/jndex.sqlite3');
$db->exec("PRAGMA journal_mode=WAL;");
$db->exec("PRAGMA busy_timeout=3600000;");
$db->exec("PRAGMA auto_vacuum=0;");

$db->beginTransaction();

$db->exec("CREATE index IF NOT EXISTS jndex_songid_artistid_artist_song_url_albumart_videoid_artistsimplified_songsimplified_filename on jndex (song_id, artist_id, artist, song, url, album_art, video_id, artist_simplified, song_simplified, filename)");
$db->exec("CREATE index IF NOT EXISTS jndex_artistid_songid_artist_song_url_albumart_videoid_artistsimplified_songsimplified_filename on jndex (artist_id, song_id, artist, song, url, album_art, video_id, artist_simplified, song_simplified, filename)");
$db->exec("CREATE index IF NOT EXISTS jndex_artist_song_artistid_songid_url_albumart_videoid_artistsimplified_songsimplified_filename on jndex (artist, song, artist_id, song_id, url, album_art, video_id, artist_simplified, song_simplified, filename)");
$db->exec("CREATE index IF NOT EXISTS jndex_artistsimplified_artist_song_artistid_songid_url_albumart_videoid_songsimplified_filename on jndex (artist_simplified, artist, song, artist_id, song_id, url, album_art, video_id, song_simplified, filename)");
$db->exec("CREATE index IF NOT EXISTS jndex_song_artistid_songid_artist_url_albumart_videoid_artistsimplified_songsimplified_filename on jndex (song, artist_id, song_id, artist, url, album_art, video_id, artist_simplified, song_simplified, filename)");
$db->exec("CREATE index IF NOT EXISTS jndex_songsimplified_artistid_songid_artist_song_url_albumart_videoid_artistsimplified_filename on jndex (song_simplified, artist_id, song_id, artist, song, url, album_art, video_id, artist_simplified, filename)");
$db->exec("CREATE index IF NOT EXISTS jndex_filename_artistid_songid_artist_song_url_albumart_videoid_artistsimplified_songsimplified on jndex (filename, artist_id, song_id, artist, song, url, album_art, video_id, artist_simplified, song_simplified)");

$db->commit();

$db->exec("analyze");

$counter = $db->query('SELECT stat from sqlite_stat1 where tbl is "jndex" limit 1');
foreach ($counter as $count) {
$keep_count = preg_split('/ /',$count[0])[0];
echo $keep_count . "\n";
}

$counter = $db->query('SELECT max(rowid) from sqlite_stat4');
foreach ($counter as $count) {
$keep_count = $count[0];
echo $keep_count . "\n";
}

?>

<?php

$db = new PDO('sqlite:jndex/jndex.sqlite3');
$db->exec("PRAGMA journal_mode=WAL;");
$db->exec("PRAGMA busy_timeout=3600000;");
$db->exec("PRAGMA auto_vacuum=0;");

echo date(DATE_COOKIE) . "\tvacuum begin\n";

$db->exec("insert into fts5(fts5) values ('optimize')");
$db->exec("vacuum");

echo date(DATE_COOKIE) . "\tvacuum end\n";

?>

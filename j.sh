#!/bin/bash
SONGMAX=2655971
if [[ "$1" =~ ^[0-9]+$ ]] ; then
if [[ "$1" -gt 0 && "$1" -le $SONGMAX ]] ; then
id="$1"
[[ -e j/$id ]] && id=""
fi ; fi
if [[ -z "$id" ]] ; then
while [[ 1 ]] ; do
id=$(( $RANDOM % 10 ))
for (( i=0; i < 6; ++i )) ; do
id=$id$(( $RANDOM % 10 ))
done
id=$(( 10#$id + 1 ))
if [[ $id -le $SONGMAX && ! -e j/$id ]] ; then break ; fi
done ; fi
echo $id
curl -q -f -g -k -L -s -I -o /dev/null -c - -A 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4449.0 Safari/537.36' https://www.jango.com/stations/1/tunein?song_id=$id | curl -q -f -g -k -L -s -b - -A 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4449.0 Safari/537.36' https://www.jango.com/streams/info --compressed -o j/$id
jq -c -s . < j/$id | php insert.php
grep -q -s ',"station_id":"1",' j/$id || rm -f -v j/$id

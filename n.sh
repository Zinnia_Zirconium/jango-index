#!/bin/bash
n="$( php n.php )"
if [[ "$n" =~ ^[1-9][0-9]*$ ]] ; then
id="$n"
else
exit 2
fi
e=4
for try in {1..4} ; do
if [[ ! -f j/$id ]] ; then
echo $id
curl -q -f -g -k -L -s -I -o /dev/null -c - -A 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4449.0 Safari/537.36' https://www.jango.com/stations/1/tunein?song_id=$id | curl -q -f -g -k -L -s -b - -A 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4449.0 Safari/537.36' https://www.jango.com/streams/info --compressed -o j/$id
jq -c -s . < j/$id | php insert.php
grep -q -s ',"song_id":'"$id"',' j/$id || rm -f -v j/$id
if [[ -f j/$id ]] ; then
e=0
break
else
e=1
id=$(( $id + 1 ))
continue
fi
else
exit 3
fi
done
exit $e

<?php
$path = pathinfo($_SERVER["SCRIPT_FILENAME"]);
header('alt-svc: h3=":443"; ma=2592000,h3-29=":443"; ma=2592000,h3-T051=":443"; ma=2592000,h3-Q050=":443"; ma=2592000,h3-Q046=":443"; ma=2592000,h3-Q043=":443"; ma=2592000,quic=":443"; ma=2592000; v="46,43"');
switch ($path["extension"]):
case "html":
header("Content-Type: text/html; charset=UTF-8");
break;
case "txt":
header("Content-Type: text/plain; charset=UTF-8");
break;
case "js":
header("Content-Type: application/javascript");
break;
case "xml":
header("Content-Type: application/xml");
break;
default:
return false;
endswitch;
ob_start();
ob_start("ob_gzhandler");
readfile($_SERVER["SCRIPT_FILENAME"]);
ob_end_flush();
header("Content-Length: " . ob_get_length());
ob_end_flush();
?>

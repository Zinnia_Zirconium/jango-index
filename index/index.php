<?php $db = new PDO('sqlite:/home/ubuntu/jndex/jndex.sqlite3'); ?>
<?php ob_start(); ob_start("ob_gzhandler"); ?>
<!DOCTYPE html>
<html lang="en">
<head>
<link rel="canonical" href="http://jango-index.tk/" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="description" content="Jango Index : Searching <?php
$counter = $db->query('SELECT max(rowid) from jndex');
$keep_count = -1;
$s = "s";
if ($counter) {
foreach ($counter as $count) {
$keep_count = $count[0];
echo $keep_count . " ";
if ($keep_count == 1) $s = "";
} }
?>song<?php echo $s; ?> on Jango Radio by artist name or song title." />
<title><?php
$title = "Jango Index";
$extra = "";
if (isset($_GET["url"]) && strlen($_GET["url"])) {
$extra = "[" . $_GET["url"] . "]";
}
if (isset($_GET["list_artist_id"]) && strlen($_GET["list_artist_id"])) {
if (strlen($extra)) {
$extra = " " . $extra;
}
$extra = "(" . $_GET["list_artist_id"] . "-)" . $extra;
}
else if (isset($_GET["artist_id"]) && strlen($_GET["artist_id"])) {
if (strlen($extra)) {
$extra = " " . $extra;
}
$extra = "(" . $_GET["artist_id"] . (isset($_GET["checkbox_artist_id"])?"-":"") . ")" . $extra;
}
if (isset($_GET["artist"]) && strlen($_GET["artist"])) {
if (strlen($extra)) {
$extra = " " . $extra;
}
$extra = "- " . $_GET["artist"] . $extra;
}
if (isset($_GET["song"]) && strlen($_GET["song"])) {
if (strlen($extra)) {
$extra = " " . $extra;
}
if (!(isset($_GET["artist"]) && strlen($_GET["artist"]))) {
$extra = " -" . $extra;
}
$extra = $_GET["song"] . $extra;
}
if (isset($_GET["list_song_id"]) && strlen($_GET["list_song_id"])) {
if (strlen($extra)) {
$extra = " " . $extra;
}
$extra = "(#" . $_GET["list_song_id"] . "-)" . $extra;
}
else if (isset($_GET["song_id"]) && strlen($_GET["song_id"])) {
if (strlen($extra)) {
$extra = " " . $extra;
}
$extra = "(#" . $_GET["song_id"] . (isset($_GET["checkbox_song_id"])?"-":"") . ")" . $extra;
}
if (isset($_GET["search"]) && strlen($_GET["search"])) {
if (strlen($extra)) {
$extra = " " . $extra;
}
$extra = $_GET["search"] . " |" . $extra;
}
if (strlen($extra)) {
$title = $extra . " : " . $title;
}
echo htmlspecialchars($title);
?></title>
<style>
body {
background-color: black;
color: white;
}
table {
position: relative;
}
a {
text-decoration: none;
color: white;
}
audio {
width: 300px;
min-height: 70px;
max-height: 70px;
}
.div-video {
background-color: black;
left: 0px;
}
video {
height: 360px;
}
.art {
visibility: hidden;
display: block;
word-wrap: break-word;
overflow-wrap: break-word;
width: 90px;
height: 90px;
min-width: 90px;
min-height: 90px;
max-width: 90px;
max-height: 90px;
}
.toggle {
  display: none;
}
.label-toggle {
  display: block;
}
.label-toggle:hover {
  display: block;
}
.label-toggle::after {
  content: ' ';
  display: inline-block;
  border-top: 5px solid transparent;
  border-bottom: 5px solid transparent;
  border-left: 5px solid currentColor;
  vertical-align: middle;
  margin-right: .7rem;
  transform: translateY(-2px);
}
.toggle:checked + .label-toggle::after {
  transform: rotate(90deg) translateX(-3px);
}
.collapsible-content {
  display: none;
}
.toggle:checked + .label-toggle + .collapsible-content {
  display: inline;
  position: absolute;
}
</style>
</head>
<body onload="cookie_onload()">
<p><a href=".">Searching <?php
echo (-1 != $keep_count) ? $keep_count : 0;
?> song<?php echo $s; ?></a></p>
<form method="get">
<table>
<tr><td><label for="search">Search</label></td><td>:</td><td><input type="text" oninput="form_oninput(this)" id="search"
name="search" value="<?php
if (isset($_GET["search"])) {
if (strlen($_GET["search"])) {
echo htmlspecialchars($_GET["search"]);
} } ?>" /></td><td><input type="submit" value="Search" /></td></tr>
<tr><td><label for="artist">Artist Name</label></td><td>:</td><td><input type="text" oninput="form_oninput(this)" id="artist"
name="artist" value="<?php
if (isset($_GET["artist"])) {
if (strlen($_GET["artist"])) {
echo htmlspecialchars($_GET["artist"]);
} } ?>" /></td><td></td></tr>
<tr><td><label for="song">Song Title</label></td><td>:</td><td><input type="text" oninput="form_oninput(this)" id="song"
name="song" value="<?php
if (isset($_GET["song"])) {
if (strlen($_GET["song"])) {
echo htmlspecialchars($_GET["song"]);
} } ?>" /></td>
<td></td></tr>
<tr><td><label for="artist_id">Artist ID</label></td><td>:</td><td><input type="text" oninput="form_oninput(this)" id="artist_id"
name="artist_id" value="<?php
if (isset($_GET["list_artist_id"]) && strlen($_GET["list_artist_id"])) {
echo htmlspecialchars($_GET["list_artist_id"]);
}
else if (isset($_GET["artist_id"]) && strlen($_GET["artist_id"])) {
echo htmlspecialchars($_GET["artist_id"]);
} ?>" /></td><td>
<input type="checkbox" <?php
if (isset($_GET["checkbox_artist_id"]) || isset($_GET["list_artist_id"]) && strlen($_GET["list_artist_id"])) {
echo "checked ";
} ?>
name="checkbox_artist_id" id="checkbox_artist_id" onchange="checkbox_onchange(this,'artist_id')" /><label for="checkbox_artist_id"> list</label>
</td></tr>
<tr><td><label for="song_id">Song ID</label></td><td>:</td><td><input type="text" oninput="form_oninput(this)" id="song_id"
name="song_id" value="<?php
if (isset($_GET["list_song_id"]) && strlen($_GET["list_song_id"])) {
echo htmlspecialchars($_GET["list_song_id"]);
}
else if (isset($_GET["song_id"]) && strlen($_GET["song_id"])) {
echo htmlspecialchars($_GET["song_id"]);
} ?>" /></td><td>
<input type="checkbox" <?php
if (isset($_GET["checkbox_song_id"]) || isset($_GET["list_song_id"]) && strlen($_GET["list_song_id"])) {
echo "checked ";
} ?>
name="checkbox_song_id" id="checkbox_song_id" onchange="checkbox_onchange(this,'song_id')" /><label for="checkbox_song_id"> list</label>
</td></tr>
<tr><td><label for="url">URL</label></td><td>:</td><td><input type="text" oninput="form_oninput(this)" id="url"
name="url" value="<?php
if (isset($_GET["url"])) {
if (strlen($_GET["url"])) {
echo htmlspecialchars($_GET["url"]);
} } ?>" /></td><td></td></tr>
</table>
</form>
<script>
function form_oninput(that){
if (that.value == "") that.name = "";
else {
if (that.id == "artist_id" && document.getElementById("checkbox_artist_id").checked) that.name = "list_artist_id";
else if (that.id == "song_id" && document.getElementById("checkbox_song_id").checked) that.name = "list_song_id";
else that.name = that.id;
} }

["search","artist","song","artist_id","song_id","url"].forEach(function(id) {
form_oninput(document.getElementById(id));
});

["checkbox_artist_id","checkbox_song_id"].forEach(function(id) {
document.getElementById(id).name="";
});

function checkbox_onchange(that,id){
if (that.checked) {
document.getElementById(id).name="list_"+id;
if (document.getElementById(id).value == "") {
document.getElementById(id).value = 1;
} } else {
if (document.getElementById(id).value == 1) {
document.getElementById(id).name = "";
document.getElementById(id).value = "";
} else {
document.getElementById(id).name=id;
} } }
</script>
<p><?php
$search = "";
$song = "";
$song_simplified = "";
$artist = "";
$artist_simplified = "";
$artist_id = "";
$song_id = "";
$url = "";
$filename = "";
$byid = false;
$byaid = false;
$new100 = true;

if (isset($_GET["list_song_id"]) && strlen($_GET["list_song_id"])) {
$new100 = false;
$byid = true;
$song_id = $_GET["list_song_id"];
}
else if (isset($_GET["song_id"]) && strlen($_GET["song_id"])) {
$new100 = false;
if (isset($_GET["checkbox_song_id"])) {
$byid = true;
}
$song_id = $_GET["song_id"];
}
if (isset($_GET["list_artist_id"]) && strlen($_GET["list_artist_id"])) {
$new100 = false;
$byaid = true;
$artist_id = $_GET["list_artist_id"];
}
else if (isset($_GET["artist_id"]) && strlen($_GET["artist_id"])) {
$new100 = false;
if (isset($_GET["checkbox_artist_id"])) {
$byaid = true;
}
$artist_id = $_GET["artist_id"];
}
if (isset($_GET["search"]) && strlen($_GET["search"])) {
$new100 = false;
$search = $_GET["search"];
}
if (isset($_GET["artist"]) && strlen($_GET["artist"])) {
$new100 = false;
$artist = preg_replace('/%/','/%',preg_replace('/_/','/_',preg_replace('/\//','//',$_GET["artist"]))) . "%";
$artist_simplified = preg_replace('/%/','/%',preg_replace('/_/','/_',preg_replace('/\//','//',preg_replace('/^the/i','',transliterator_transliterate('Latin-ASCII; [\u0000-\u002f\u003a-\u0040\u005b-\u0060\u007b-\uffff] remove',$_GET["artist"]))))) . "%";
if ($artist_simplified == "%") {
$artist_simplified = $artist;
}
}
if (isset($_GET["url"]) && strlen($_GET["url"])) {
$new100 = false;
$url = "%" . preg_replace('/%/','/%',preg_replace('/_/','/_',preg_replace('/\//','//',preg_replace('/^[^:]*:/','',$_GET["url"])))) . "%";
preg_match('/[^\/]*$/',preg_replace('/^[^\/]*\/\//','',$_GET["url"]),$filename);
$filename = preg_replace('/%/','/%',preg_replace('/_/','/_',preg_replace('/\//','//',$filename[0]))) . "%";
if ($filename == "%") {
$filename = $url;
}
}
if (isset($_GET["song"]) && strlen($_GET["song"])) {
$new100 = false;
$song = preg_replace('/%/','/%',preg_replace('/_/','/_',preg_replace('/\//','//',$_GET["song"]))) . "%";
$song_simplified = preg_replace('/%/','/%',preg_replace('/_/','/_',preg_replace('/\//','//',preg_replace('/^the/i','',transliterator_transliterate('Latin-ASCII; [\u0000-\u002f\u003a-\u0040\u005b-\u0060\u007b-\uffff] remove',$_GET["song"]))))) . "%";
if ($song_simplified == "%") {
$song_simplified = $song;
}
}

$slowscan = false;
$result = -1;
while (1) {

$andand = false;
$finalsort = false;
$where = 0;
$select = 'select url, jndex.artist, artist_id, jndex.song, ' . ($byaid?'min(song_id) as ':'') . 'song_id, album_art, video_id from jndex ';
if ($new100) {
$select .= 'order by song_id desc limit 100';
} else {
if (strlen($search)) {
$select .= 'join fts5 on jndex.rowid is fts5.rowid ';
}
$select .= 'where ';
if (strlen($search)) {
++$where;
$select .= '(fts5 match :search) ';
$finalsort = !$byaid;
$andand = true;
}
if (strlen($artist)) {
++$where;
if ($andand) {
$select .= 'and ';
}
$select .= '(jndex.artist like :artist escape "/" or artist_simplified like :artist_simplified escape "/") ';
$andand = true;
}
if (strlen($song)) {
++$where;
if ($andand) {
$select .= 'and ';
}
$select .= '(jndex.song like :song escape "/" or song_simplified like :song_simplified escape "/") ';
$finalsort = !$byaid;
$andand = true;
}
if (strlen($artist_id)) {
++$where;
if ($andand) {
$select .= 'and ';
}
$select .= '(artist_id ' . ($byaid?'>=':'is') . ' :artist_id) ';
$finalsort = !$byaid;
$andand = true;
}
if (strlen($song_id)) {
++$where;
if ($andand) {
$select .= 'and ';
}
$select .= '(song_id ' . ($byid?'>=':'is') . ' :song_id) ';
$andand = true;
}
if (strlen($url)) {
if ($andand) {
$select .= 'and ';
}
if ($slowscan) {
$select .= '(url like :url escape "/") ';
if (!$andand) {
$select .= 'order by artist, song ';
} } else {
$select .= '(filename like :filename escape "/") ';
if ($url == $filename) {
if (!$andand) {
$select .= 'order by artist, song ';
} } else {
++$where;
$finalsort = !$byaid;
} } }
if ($byaid) {
$select .= 'group by artist_id ';
}
$select .= 'limit 250';
}

if (isset($_GET["select"])) {
echo htmlspecialchars($select) . '</p><p>';
}

$stmt = $db->prepare($select);
if (!$stmt) break;

if (!$new100) {
if (strlen($search)) {
$stmt->bindParam(':search', $search);
}
if (strlen($song_id)) {
$stmt->bindParam(':song_id', $song_id);
}
if (strlen($artist_id)) {
$stmt->bindParam(':artist_id', $artist_id);
}
if (strlen($artist)) {
$stmt->bindParam(':artist', $artist);
$stmt->bindParam(':artist_simplified', $artist_simplified);
}
if (strlen($song)) {
$stmt->bindParam(':song', $song);
$stmt->bindParam(':song_simplified', $song_simplified);
}
if (strlen($url)) {
if ($slowscan) {
$stmt->bindParam(':url', $url);
} else {
$stmt->bindParam(':filename', $filename);
} } }

$stmt->execute();
$result = $stmt->fetchAll();

if (!$slowscan && count($result) == 0 && strlen($url)) {
$slowscan = true;
continue;
} break;
}

if (-1 != $result) {

if ($byid) {
if ($where > 1) {
usort($result,function (array $a, array $b) { return ($a["song_id"] <=> $b["song_id"]); });
if (isset($_GET["select"])) {
echo 'Sorted by Song ID</p><p>';
} }
$finalsort = false;
}
if ($finalsort) {
usort($result,function (array $a, array $b) {
$r = strnatcasecmp($a['artist'],$b['artist']);
if ($r) return $r;
$r = strnatcasecmp($a['song'],$b['song']);
if ($r) return $r;
$r = $a['artist_id'] <=> $b['artist_id'];
if ($r) return $r;
return $a['song_id'] <=> $b['song_id'];
});
if (isset($_GET["select"])) {
echo 'Sorted by Artist Name, Song Title, Artist ID, Song ID</p><p>';
} }
$count = count($result);
echo $count . " result" . ($count == 1 ? "" : "s") . "</p>\n";
if ($count) {
$repeat = 0;
if (isset($_COOKIE["repeat"])) {
$repeat = $_COOKIE["repeat"];
}
echo "<p id=\"repeat_p\" style=\"display:none\"><script>document.getElementById('repeat_p').style.display = ''</script>Repeat : <select id=\"repeat\" onchange=\"cookie_onchange(this)\"><option" . (($repeat == 0) ? " selected" : "" ) .">None</option><option" . (($repeat == 1) ? " selected" : "" ) . ">One</option><option" . (($repeat == 2) ? " selected" : "" ) . ">All</option></select></p>\n";
}
$z = 250;
for ($i = 0; $i < $count; ++$i) {
$prev = ($i == 0) ? ($count - 1) : ($i - 1);
$next = ($i == $count - 1) ? 0 : ($i + 1);
$info = $result[$i];
$title = $info["song"] . " - " . $info["artist"] . " : Jango Index";
$unique = htmlspecialchars($info["song_id"]);
$unique_prev = htmlspecialchars($result[$prev]["song_id"]);
$unique_next = htmlspecialchars($result[$next]["song_id"]);
echo "<table>\n<tr><td id=\"artwork-" . $unique . "\" style=\"display:none\" rowspan=\"" . ((strlen($info["video_id"])) ? 6 : 5) . "\"><script>document.getElementById('artwork-" . $unique . "').style.display=\"\"</script>\n<img onload=\"this.style.visibility='visible'\" class=\"art lazy\" alt=\"" . htmlspecialchars($info["artist"]) . "\" title=\"" . htmlspecialchars($info["artist"]) . "\" \ndata-src=\"" . htmlspecialchars($info["album_art"]) . "\" /></td>\n";
echo "<td><input id=\"collapsible-" . $unique . "\" class=\"toggle\" type=\"checkbox\" autocomplete=\"off\">\n";
echo "<label for=\"collapsible-" . $unique . "\" class=\"label-toggle\" onclick=\"preload_onclick(" . $unique . ")\">";
echo "URL&nbsp;&nbsp;</label>\n<div style=\"z-index: " . htmlspecialchars($z+250) . "\" class=\"collapsible-content div-audio\">\n<audio playsinline controls preload=\"" . (($i==0)?"metadata":"none") . "\" id=\"audio-" . $unique . "\"" . (($i!=0)?" style=\"display:none\"":"") . "\ntitle=\"" . htmlspecialchars($title) . "\" \n";
echo "onplay=\"mediasession_onplay(this," . htmlspecialchars(json_encode(array("title" => $info["song"],"artist" => $info["artist"],"artwork" => array(array("sizes" => "320x180","src" => $info["album_art"]))),JSON_UNESCAPED_SLASHES)) . ",false," . $unique . "," . $unique_prev . "," . $unique_next . ")\" ontimeupdate=\"mediasession_progressbar(this)\" onended=\"repeat_onended(this,false," . $unique . "," . $unique_next . ")\"\n";
echo "untouched " . (($i==0)?"loadsrc=\"0\" ":"load") . "src=\"" . htmlspecialchars($info["url"]) . "\"></audio>";
if ($i!=0) echo "\n<noscript><audio controls playsinline preload=\"none\"\ntitle=\"" . htmlspecialchars($title) . "\"\nsrc=\"" . htmlspecialchars($info["url"]) . "\"></audio></noscript>";
echo "</div>\n</td><td>:</td><td><a href=\"" . htmlspecialchars($info["url"]) . "\" ";
echo "target=\"_blank\" \nrel=\"nofollow noopener noreferrer\">" . htmlspecialchars($info["url"]) . "</a></td></tr>\n";
echo "<tr><td>Artist&nbsp;Name</td><td>:</td><td>" . htmlspecialchars($info["artist"]) . "</td></tr>\n";
echo "<tr><td>Song&nbsp;Title</td><td>:</td><td>" . htmlspecialchars($info["song"]) . "</td></tr>\n";
echo "<tr><td>Artist ID</td><td>:</td><td>" . htmlspecialchars($info["artist_id"]) . "</td></tr>\n";
echo "<tr><td>Song ID</td><td>:</td><td>" . $unique . "</td></tr>\n";
if (strlen($info["video_id"])) {
echo "<tr><td>\n<input id=\"collapsible-" . $unique . "-video\" class=\"toggle\" type=\"checkbox\" autocomplete=\"off\">\n";
echo "<label for=\"collapsible-" . $unique . "-video\" class=\"label-toggle\" onclick=\"video_preload(" . $unique . ",'" . htmlspecialchars($info["video_id"]) .  "')\">";
echo "Video&nbsp;ID&nbsp;&nbsp;</label>\n<div style=\"z-index: " . htmlspecialchars($z) . "\" class=\"collapsible-content div-video\">\n<video playsinline controls preload=\"none\" id=\"video-" . $unique . "\"\ntitle=\"" . htmlspecialchars($title) . "\" \n";
echo "onplay=\"mediasession_onplay(this," . htmlspecialchars(json_encode(array("title" => $info["song"],"artist" => $info["artist"],"artwork" => array(array("sizes" => "320x180","src" => "//i.ytimg.com/vi/" . $info["video_id"] . "/mqdefault.jpg"))),JSON_UNESCAPED_SLASHES)) . ",true," . $unique . "," . $unique_prev . "," . $unique_next . ")\" ontimeupdate=\"mediasession_progressbar(this)\" onended=\"repeat_onended(this,true," . $unique . "," . $unique_next . ")\"\n";
echo "src=\"//ox-proxy.gq/youtube/" . htmlspecialchars($info["video_id"]) . "\"></video></div>\n";
echo "</td><td>:</td><td>";
echo "<a href=\"//youtu.be/" . htmlspecialchars($info["video_id"]) . "\" \ntarget=\"_blank\" rel=\"nofollow noopener noreferrer\">" . htmlspecialchars($info["video_id"]) . "</a>";
echo "</td></tr>\n";
}
echo "</table>\n<p></p>\n";
--$z;
} }
?>
<script>
function cookie_onload() {
var that = document.getElementById('repeat');
if (that) {
cookie_onchange(that);
} }

var interval;

function cookie_onchange(that){
document.cookie='repeat='+that.selectedIndex;
if (navigator.userAgent.match(/android/i)) {
loop_every(that.selectedIndex==1);
}
if (that.selectedIndex==0){
if (interval) {
clearInterval(interval);
interval = undefined;
} } else {
if (!interval) {
if (!('mediaSession' in navigator)) {
if (navigator.userAgent.match(/cpu.*os (1[0-4]_[0-9]).*applewebkit.*mobile/i)) {
interval = setInterval(keep_playing,1000);
} } } } }

function keep_playing (){
if (document.hidden) {
if (last_played) {
if (last_played.paused) {
if (!last_played.ended) {
last_played.play();
} } } } }

var last_played;
var tit;

function preload_onclick(uniq){
sanDimas_preload();
if (load_src(document.getElementById('audio-'+uniq))){
if (!tit) {
document.title = document.getElementById('audio-'+uniq).title;
document.getElementById('audio-'+uniq).play();
} } }

function load_src(that){
if (that.getAttribute("loadsrc")) {
if (that.getAttribute("loadsrc")!=="0"){
that.style.display = "";
that.src=that.getAttribute("loadsrc"); }
that.removeAttribute("loadsrc");
return true;
} }

function mediasession_onplay(that,meta,vide,uniq,prev,next){
if (that.hasAttribute("untouched")) {
that.removeAttribute("untouched");
} else {
if (last_played){
if (last_played.id!=that.id){
if (!last_played.ended){
if (!last_played.paused){
if (last_played.id.match(/video/)) last_played.muted=true;
last_played.pause();
} } } }
last_played=that;
that.muted=!tit;
document.getElementById('collapsible-'+uniq+(vide?'-video':'')).checked=true;
if ('mediaSession' in navigator) {navigator.mediaSession.metadata = new MediaMetadata(meta);
navigator.mediaSession.setActionHandler('seekbackward',function(){document.getElementById((vide?'video-':'audio-')+uniq).currentTime-=7});
navigator.mediaSession.setActionHandler('seekforward',function(){document.getElementById((vide?'video-':'audio-')+uniq).currentTime+=7});
navigator.mediaSession.setActionHandler('seekto',function(deet){
if (deet.fastSeek && "fastSeek" in document.getElementById((vide?'video-':'audio-')+uniq))
document.getElementById((vide?'video-':'audio-')+uniq).fastSeek(deet.seekTime);
else document.getElementById((vide?'video-':'audio-')+uniq).currentTime=deet.seekTime});
navigator.mediaSession.setActionHandler('previoustrack',function(){document.getElementById((vide?'video-':'audio-')+uniq).pause();
document.getElementById('collapsible-'+uniq+(vide?'-video':'')).checked=false;
load_src(document.getElementById('audio-'+prev));
document.getElementById('audio-'+prev).play();
document.getElementById('audio-'+prev).currentTime=0;
document.getElementById('collapsible-'+prev).checked=true});
navigator.mediaSession.setActionHandler('nexttrack',function(){
document.getElementById((vide?'video-':'audio-')+uniq).pause();
document.getElementById('collapsible-'+uniq+(vide?'-video':'')).checked=false;
load_src(document.getElementById('audio-'+next));
document.getElementById('audio-'+next).play();
document.getElementById('audio-'+next).currentTime=0;
document.getElementById('collapsible-'+next).checked=true})}
document.title = that.title;
} }

function mediasession_progressbar (that) {
if (!tit) {
if (that.currentTime > 0) {
tit = true;
that.pause();
that.muted=false;
} }
if ('mediaSession' in navigator) {
if ("setPositionState" in navigator.mediaSession) {
if (that.seekable.length) {
navigator.mediaSession.setPositionState({
duration: that.duration,
playbackRate: Number.MIN_VALUE,
position: that.currentTime
}); } } } }

function repeat_onended(that,vide,uniq,next){
if (document.getElementById('repeat').selectedIndex!=2){that.play()};
if (document.getElementById('repeat').selectedIndex==0){that.pause()};
if(document.getElementById('repeat').selectedIndex==2){
if (navigator.userAgent.match(/cpu.*os (12_[2-9]).*applewebkit.*mobile/i)) {
mute_except((vide?'video-':'audio-')+uniq,'audio-'+next);
}
document.getElementById('collapsible-'+uniq+(vide?'-video':'')).checked=false;
load_src(document.getElementById('audio-'+next));
document.getElementById('audio-'+next).play();
document.getElementById('audio-'+next).currentTime=0;
document.getElementById('collapsible-'+next).checked=true}
}

var sanDimasRunning;

function sanDimas_preload () {
if (!sanDimasRunning || audioContext == false) {
sanDimasRunning = true;
if (navigator.userAgent.match(/cpu.*os (12_[2-9]|1[34]_[0-9]).*applewebkit.*mobile/i)) {
sanDimas_setup();
preload_audio();
} else if (navigator.userAgent.match(/cpu.*os (1[0-4]_[0-9]).*applewebkit.*mobile/i)) {
preload_audio();
sanDimas_setup();
} else {
preload_audio();
} } }

var audioContext, audioBuffer, tone, gain;
var sanDimas;

function sanDimas_setup () {
if (window.AudioContext || window.webkitAudioContext) {
if (!audioContext) audioContext = new (window.AudioContext || window.webkitAudioContext)();
if (audioContext.resume) audioContext.resume();
audioContext.onstatechange = statechange_action;
var channels = 2;
var frameCount = audioContext.sampleRate * 3;
audioBuffer = audioContext.createBuffer(channels, frameCount, audioContext.sampleRate);
for (var channel = 0; channel < channels; channel++) {
var nowBuffering = audioBuffer.getChannelData(channel);
for (var i = 0; i < frameCount; i++) {
nowBuffering[i] = 0;
} }
gain = audioContext.createGain();
<?php
if (isset($_GET["gain"])) {
echo "gain.gain.value = 0.1;\n";
} else {
echo "gain.gain.value = 0;\n";
}
?>
gain.connect(audioContext.destination);
tone = audioContext.createOscillator();
tone.connect(gain);
tone.frequency.value = 2600;
tone.start();
sanDimas = audioContext.createBufferSource();
sanDimas.buffer = audioBuffer;
sanDimas.connect(audioContext.destination);
sanDimas.loop = true;
sanDimas.start();
} }

function statechange_action () {
if (audioContext) {
if (audioContext.state) {
if ((audioContext.state == "interrupted") || (audioContext.state == "suspended")) {
audioContext.resume();
} } } }

function preload_audio () {
var all = document.getElementsByTagName("audio");
var tot = all.length;
for ( var i = 0; i < tot; ++i) {
all[i].play();
all[i].pause();
} }

function mute_except (id1,id2) {
var all = document.getElementsByTagName("audio");
var tot = all.length;
var i;
for (i = 0; i < tot; ++i) {
if (all[i].id!=id1){
if (all[i].id!=id2){
all[i].pause();
all[i].muted=true;
} } }
all = document.getElementsByTagName("video");
tot = all.length;
for (i = 0; i < tot; ++i) {
if (all[i].id!=id1){
if (all[i].id!=id2){
all[i].pause();
all[i].muted=true;
} } } }

function loop_every (w) {
var all = document.getElementsByTagName("audio");
var tot = all.length;
var i;
for (i = 0; i < tot; ++i) {
all[i].loop=w;
}
all = document.getElementsByTagName("video");
tot = all.length;
for (i = 0; i < tot; ++i) {
all[i].loop=w;
} }

function video_preload(uniq,vid) {
sanDimas_preload();
tit = true;
document.title = document.getElementById('video-'+uniq).title;
if (!document.getElementById('video-'+uniq).poster) {
document.getElementById('video-'+uniq).poster="//i.ytimg.com/vi/"+vid+"/hqdefault.jpg";
} }

document.addEventListener("DOMContentLoaded", function() {
  let lazyImages = [].slice.call(document.querySelectorAll("img.lazy"));
  let active = false;

  const lazyLoad = function() {
    if (active === false) {
      active = true;

      setTimeout(function() {
        lazyImages.forEach(function(lazyImage) {
          if ((lazyImage.getBoundingClientRect().top <= window.innerHeight && lazyImage.getBoundingClientRect().bottom >= 0) && getComputedStyle(lazyImage).display !== "none") {
            lazyImage.src = lazyImage.dataset.src;
            lazyImage.classList.remove("lazy");

            lazyImages = lazyImages.filter(function(image) {
              return image !== lazyImage;
            });

            if (lazyImages.length === 0) {
              document.removeEventListener("scroll", lazyLoad);
              window.removeEventListener("resize", lazyLoad);
              window.removeEventListener("orientationchange", lazyLoad);
              window.removeEventListener("load", lazyLoad);
            }
          }
        });

        active = false;
      }, 200);
    }
  };

  document.addEventListener("scroll", lazyLoad);
  window.addEventListener("resize", lazyLoad);
  window.addEventListener("orientationchange", lazyLoad);
  window.addEventListener("load", lazyLoad);
});
</script>
</body>
</html>
<?php
ob_end_flush();
header("Content-Length: " . ob_get_length());
header('alt-svc: h3=":443"; ma=2592000,h3-29=":443"; ma=2592000,h3-T051=":443"; ma=2592000,h3-Q050=":443"; ma=2592000,h3-Q046=":443"; ma=2592000,h3-Q043=":443"; ma=2592000,quic=":443"; ma=2592000; v="46,43"');
ob_end_flush();
?>

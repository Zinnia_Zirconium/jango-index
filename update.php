<?php

$db = new PDO('sqlite:jndex/jndex.sqlite3');
$db->exec("PRAGMA journal_mode=WAL;");
$db->exec("PRAGMA busy_timeout=3600000;");
$db->exec("PRAGMA auto_vacuum=0;");

$json = json_decode(file_get_contents("php://stdin"),true);

$update = "update jndex set video_id = :video_id where song_id = :song_id ";
$stmt = $db->prepare($update);

$stmt->bindParam(':song_id', $song_id);
$stmt->bindParam(':video_id', $video_id);

$db->beginTransaction();

foreach ($json as &$info) {
    $song_id = $info['song_id'];
    $video_id = $info['video_id'];

    $stmt->execute();
}

$db->commit();

?>
